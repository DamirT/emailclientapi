package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.converter.AccountConverter;
import api.dto.AccountDTO;
import api.dto.MessageDTO;
import api.entity.Account;
import api.entity.Message;
import api.entity.User;
import api.service.AccountServiceInterface;
import api.service.MessageServiceInterface;
import api.service.UserServiceInterface;

@RestController
@RequestMapping(value="api/users/{userId}/accounts")
public class AccountController {

	@Autowired
	private AccountServiceInterface accountService;
	
	@Autowired
	private UserServiceInterface userService;
	
	@Autowired
	private MessageServiceInterface messageService;
	
	@Autowired
	private AccountConverter converter;

	
	@GetMapping
	public ResponseEntity<List<AccountDTO>> getAccountsByUser(@PathVariable("userId") Integer userId) {
		
		List<Account> accounts = accountService.findAllByUser(userId);
		List<AccountDTO> accountsDTO = new ArrayList<AccountDTO>();
		for (Account account : accounts) {
			accountsDTO.add(converter.toDTO(account));
		}
		return new ResponseEntity<List<AccountDTO>>(accountsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<AccountDTO> getAccount(@PathVariable("id") Integer id) {
		Account account = accountService.findOne(id);
		if(account == null) {
			return new ResponseEntity<AccountDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<AccountDTO>(converter.toDTO(account), HttpStatus.OK);
	}
	
	
	@GetMapping(value="/{idAcc}/messages")
	public ResponseEntity<List<MessageDTO>> getMessagesByAccount(
			@PathVariable("idAcc") Integer accId,
			@RequestParam("maxId") Integer maxId) {
		
		List<Message> messages = messageService.findOnlyNewMessages(accId, maxId);
		List<MessageDTO> messagesDTO = new ArrayList<MessageDTO>();
		for (Message message : messages) {
			messagesDTO.add(new MessageDTO(message));
		}
		return new ResponseEntity<List<MessageDTO>>(messagesDTO, HttpStatus.OK);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<AccountDTO> saveAccount(@RequestBody AccountDTO accountDTO, @PathVariable("userId") Integer userId) {
		Account account = converter.toAccount(accountDTO);
		User user = userService.findOne(userId);
		if(user == null) {
			return new ResponseEntity<AccountDTO>(HttpStatus.BAD_REQUEST);
		}
		account.setUser(user);
		account = accountService.save(account);
		return new ResponseEntity<AccountDTO>(converter.toDTO(account), HttpStatus.CREATED);
	}
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<AccountDTO> updateAccount(
			@RequestBody AccountDTO accountDTO, 
			@PathVariable("id") Integer accId, 
			@PathVariable("userId") Integer userId) {
		
		if(!accId.equals(accountDTO.getId())) {
			return new ResponseEntity<AccountDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Account account = accountService.findOne(accId);
		if(account == null) {
			return new ResponseEntity<AccountDTO>(HttpStatus.BAD_REQUEST);
		}
		Account izmenjenAcc = converter.toAccount(accountDTO);
		izmenjenAcc.setUser(userService.findOne(userId));
		account = accountService.save(izmenjenAcc);
		return new ResponseEntity<AccountDTO>(converter.toDTO(account), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteAccount(@PathVariable("id") Integer id) {
		Account account = accountService.findOne(id);
		if(account == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		accountService.remove(account);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}
