package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.dto.AttachmentDTO;
import api.entity.Attachment;
import api.entity.Message;
import api.service.AttachmentServiceInterface;
import api.service.MessageServiceInterface;

@RestController
@RequestMapping(value="api/messages/{idMess}/attachments")
public class AttachmentController {
	
	@Autowired
	private AttachmentServiceInterface attachmentService;
	
	@Autowired
	private MessageServiceInterface messageService;
	
	@GetMapping
	public ResponseEntity<List<AttachmentDTO>> getAttachmentsByMessage(@PathVariable("idMess") Integer idMess) {
		List<Attachment> attachments = attachmentService.findAllByMessage(idMess);
		List<AttachmentDTO> attachmentsDTO = new ArrayList<AttachmentDTO>();
		for (Attachment a : attachments) {
			//moguce da cemo morati praviti attachmentLighDTO-ove
			attachmentsDTO.add(new AttachmentDTO(a));
		}
		return new ResponseEntity<List<AttachmentDTO>>(attachmentsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<AttachmentDTO> getAttachment(@PathVariable("id") Integer id){
		Attachment attachment = attachmentService.findOne(id);
		if(attachment == null){
			return new ResponseEntity<AttachmentDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<AttachmentDTO>(new AttachmentDTO(attachment), HttpStatus.OK);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<AttachmentDTO> saveAttachment(@RequestBody AttachmentDTO attachmentDTO, @PathVariable("idMess") Integer idMess){
		Attachment attachment = attachmentDTO.convertToAttachment();
		Message message = messageService.findOne(idMess);
		if(message == null) {
			return new ResponseEntity<AttachmentDTO>(HttpStatus.BAD_REQUEST);
		}
		
		attachment.setMessage(message);
		attachment = attachmentService.save(attachment);
		return new ResponseEntity<AttachmentDTO>(new AttachmentDTO(attachment), HttpStatus.CREATED);	
	}
	
	//NISMO SIGURNI DA NAM TREBA, AKO TREBA MORA SE ODRADITI MODIFIKACIJA
//	@PutMapping(value="/{id}", consumes="application/json")
//	public ResponseEntity<AttachmentDTO> updateAttachment(@RequestBody AttachmentDTO attachmentDTO, @PathVariable("id") Integer id){
//		
//	
//		Attachment attachment = attachmentService.findOne(id); 
//		if (attachment == null) {
//			return new ResponseEntity<AttachmentDTO>(HttpStatus.BAD_REQUEST);
//		}
//		
//		attachment.setData(attachmentDTO.getData());
//		attachment.setMime_type(attachmentDTO.getMime_type());
//		attachment.setName(attachmentDTO.getName());
//	
//		attachment = attachmentService.save(attachment);
//		
//		return new ResponseEntity<AttachmentDTO>(new AttachmentDTO(attachment), HttpStatus.OK);	
//	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteAttachment(@PathVariable("id") Integer id){
		Attachment attachment = attachmentService.findOne(id);
		if (attachment != null){
			attachmentService.remove(attachment);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
}
	


