package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.dto.ContactDTO;
import api.entity.Contact;
import api.entity.User;
import api.service.ContactServiceInterface;
import api.service.UserServiceInterface;

@RestController
@RequestMapping(value="api/users/{idUser}/contacts")
public class ContactController {

	@Autowired
	private ContactServiceInterface contactService;
	
	
	@Autowired
	private UserServiceInterface userService;
	
	@GetMapping
	public ResponseEntity<List<ContactDTO>> getContactsByUser(@PathVariable("idUser") Integer id) {
		List<Contact> contacts = contactService.findAllByUser(id);
		List<ContactDTO> contactsDTO = new ArrayList<>();
		for (Contact contact : contacts) {
			contactsDTO.add(new ContactDTO(contact));
		}
		return new ResponseEntity<List<ContactDTO>>(contactsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<ContactDTO> getContact(@PathVariable("id") Integer id) {
		Contact contact = contactService.findOne(id);
		if(contact == null) {
			return new ResponseEntity<ContactDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.OK);
	}
	
	@PostMapping(consumes = "application/json")
	public ResponseEntity<ContactDTO> saveContact(@RequestBody ContactDTO contactDTO, @PathVariable("idUser") Integer userId) {
		Contact contact = contactDTO.convertToContact();
		User user = userService.findOne(userId);
		if(user == null) {
			return new ResponseEntity<ContactDTO>(HttpStatus.BAD_REQUEST);
		}
		contact.setUser(user);
		contact = contactService.save(contact);
		return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.CREATED);
	}
	
	@PutMapping(value="/{id}", consumes = "application/json")
	public ResponseEntity<ContactDTO> updateContact(
			@RequestBody ContactDTO contactDTO, 
			@PathVariable("id") Integer contactId, 
			@PathVariable("idUser") Integer userId) {
		
		if(!contactId.equals(contactDTO.getId())) {
			return new ResponseEntity<ContactDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Contact contact = contactService.findOne(contactId);
		if(contact == null) {
			return new ResponseEntity<ContactDTO>(HttpStatus.BAD_REQUEST);
		}
		
		
		Contact izmenjenContact = contactDTO.convertToContact();
		izmenjenContact.setUser(userService.findOne(userId));
		contact = contactService.save(izmenjenContact);
		return  new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteContact(@PathVariable("id") Integer id) {
		Contact contact = contactService.findOne(id);
		if (contact == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		contactService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	
}
