package api.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import api.converter.FolderConverter;
import api.dto.FolderDTO;
import api.entity.Account;
import api.entity.Folder;
import api.service.AccountServiceInterface;
import api.service.FolderServiceInterface;

@RestController
@RequestMapping("api/accounts/{idAcc}/folders")
public class FolderController {
	
	@Autowired
	private FolderServiceInterface folderService;
	
	@Autowired
	private AccountServiceInterface accountService;
	
	@Autowired
	private FolderConverter converter;
	
	@GetMapping
	public ResponseEntity<List<FolderDTO>> getFoldersByAcc(
			@PathVariable("idAcc") Integer idAcc, 
			@RequestParam(required = false) Integer parentFolderId){

		List<Folder> folders = folderService.findAllByAcc(idAcc);
		List<Folder> outputFolders = new ArrayList<Folder>();
		for (Folder folder : folders) {
			if(parentFolderId == null && folder.getParent() == null) {	
				outputFolders.add(folder);
			} else if(parentFolderId != null && folder.getParent() != null && parentFolderId.equals(folder.getParent().getId())){
				outputFolders.add(folder);
			}
		}
		
		List<FolderDTO> foldersDTO = new ArrayList<FolderDTO>();
		for (Folder folder : outputFolders) {
			foldersDTO.add(converter.toDTO(folder));
		}
		return new ResponseEntity<List<FolderDTO>>(foldersDTO, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<FolderDTO> getFolder(@PathVariable("id") Integer id){
		Folder folder = folderService.findOne(id);
		if (folder == null) {
			return new ResponseEntity<FolderDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<FolderDTO>(converter.toDTO(folder), HttpStatus.OK);
	}
	
	@PostMapping(consumes = "application/json")
	public ResponseEntity<FolderDTO> saveFolder(@RequestBody FolderDTO folderDTO, @PathVariable("idAcc") Integer idAcc){
		Folder folder =  converter.toFolder(folderDTO);
		Account account = accountService.findOne(idAcc);
		if(account == null) {
			return new ResponseEntity<FolderDTO>(HttpStatus.BAD_REQUEST);
		}
		folder.setAccount(account);
		folder = folderService.save(folder);
		return new ResponseEntity<FolderDTO>(converter.toDTO(folder), HttpStatus.CREATED);
	}
	
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<FolderDTO> updateFolder(
			@RequestBody FolderDTO folderDTO, 
			@PathVariable("id") Integer folderId,
			@PathVariable("idAcc") Integer accId){
		
		// ID ne sme biti null i mora se poklapati sa ID-jem foldera koji je stigao
		if(folderId==null || !folderId.equals(folderDTO.getId())) {
			return new ResponseEntity<FolderDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Folder folder = folderService.findOne(folderId);
		if (folder == null) {
			return new ResponseEntity<FolderDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Folder izmenjenFolder = converter.toFolder(folderDTO);
		izmenjenFolder.setAccount(accountService.findOne(accId));
		izmenjenFolder.setMessages(folder.getMessages());
		folder = folderService.save(izmenjenFolder);
		
		return new ResponseEntity<FolderDTO>(converter.toDTO(folder), HttpStatus.OK);
	}
	
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteFolder(@PathVariable("id") Integer id){
		Folder folder = folderService.findOne(id);
		if (folder != null){
			folderService.remove(folder);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

}
