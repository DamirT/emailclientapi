package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.dto.MessageDTO;
import api.entity.Account;
import api.entity.Folder;
import api.entity.Message;
import api.entity.Tag;
import api.service.AccountServiceInterface;
import api.service.FolderServiceInterface;
import api.service.MessageServiceInterface;
import api.service.TagServiceInterface;

@RestController
@RequestMapping(value="api/folders/{idFolder}/messages")
public class MessageController {
	
	@Autowired
	private MessageServiceInterface messageService;
	
	@Autowired
	private AccountServiceInterface accountService;
	
	@Autowired
	private FolderServiceInterface folderService;
	
	@Autowired
	private TagServiceInterface tagService;
	
	@GetMapping
	public ResponseEntity<List<MessageDTO>> getMessagesByFolder(@PathVariable("idFolder") Integer idFolder ) {
		List<Message> messages = messageService.findAllByFolder(idFolder);
		List<MessageDTO> messagesDTO = new ArrayList<MessageDTO>();
		for (Message m : messages) {
			messagesDTO.add(new MessageDTO(m));
		}
		return new ResponseEntity<List<MessageDTO>>(messagesDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<MessageDTO> getMessage(@PathVariable("id") Integer id){
		Message message = messageService.findOne(id);
		if(message == null){
			return new ResponseEntity<MessageDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<MessageDTO> saveMessage(@RequestBody MessageDTO messageDTO, @PathVariable("idFolder") Integer idFolder){
		Message message = messageDTO.convertToMessage();		
		Folder folder = folderService.findOne(idFolder);
		if(folder == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
				
		Account account = accountService.findOne(folder.getAccount().getId());
		if(account == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
				
		message.setFolder(folder);
		message.setAccount(account);
		message = messageService.saveAndSend(message);
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.CREATED);	
	}
	
	@PostMapping(value="/drafts", consumes="application/json")
	public ResponseEntity<MessageDTO> saveMessageToDrafts(@RequestBody MessageDTO messageDTO, @PathVariable("idFolder") Integer idFolder){
		Message message = messageDTO.convertToMessage();		
		Folder folder = folderService.findOne(idFolder);
		if(folder == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
				
		Account account = accountService.findOne(folder.getAccount().getId());
		if(account == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
				
		message.setFolder(folder);
		message.setAccount(account);
		message = messageService.saveDraftsMessage(message);
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.CREATED);	
	}
	
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<MessageDTO> updateMessage(
			@RequestBody MessageDTO messageDTO,
			@PathVariable("id") Integer messageId,
			@PathVariable("idFolder") Integer idFolder){
		
		if(!messageId.equals(messageDTO.getId())) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Message message = messageService.findOne(messageId); 
		if (message == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
		
		//moram prvo da je uklonim iz liste poruka u postojeceg foldera (drafts)
		message.getFolder().getMessages().remove(message);
		Message izmenjenMessage = messageDTO.convertToMessage();
		Folder folder = folderService.findOne(idFolder);
		//pa joj setujem adekvatan folder (sent)
		izmenjenMessage.setFolder(folder);
		izmenjenMessage.setAccount(accountService.findOne(folder.getAccount().getId()));
		message = messageService.saveAndSend(izmenjenMessage);
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);	
	}
	
	
	//za dodavanje taga u message
	@PutMapping(value="/{id}/tags/{idTag}")
	public ResponseEntity<MessageDTO> addTag(
			@PathVariable("id") Integer messageId,
			@PathVariable("idTag") Integer idTag){
	
		Message message = messageService.findOne(messageId); 
		if (message == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Tag tag = tagService.findOne(idTag);
		if (tag == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
		message.getTags().add(tag);
		tag.getMessages().add(message);
		message = messageService.save(message);
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);	
	}
	
	
	//za uklanjanje taga iz message
	@DeleteMapping(value="/{id}/tags/{idTag}")
	public ResponseEntity<MessageDTO> removeTag(
			@PathVariable("id") Integer messageId,
			@PathVariable("idTag") Integer idTag){
	
		Message message = messageService.findOne(messageId); 
		if (message == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}		
		Tag tag = tagService.findOne(idTag);
		if (tag == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
		message.getTags().remove(tag);
		tag.getMessages().remove(message);
		message = messageService.save(message);
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);	
	}
	
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteMessage(@PathVariable("id") Integer id){
		Message message = messageService.findOne(id);
		if (message != null){
			messageService.remove(message);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
}
	


