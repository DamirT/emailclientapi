package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.dto.TagDTO;
import api.entity.Tag;
import api.entity.User;
import api.service.TagServiceInterface;
import api.service.UserServiceInterface;

@RestController
@RequestMapping(value="api/users/{idUser}/tags")
public class TagController {
	@Autowired
	private TagServiceInterface tagService;
	
	@Autowired
	private UserServiceInterface userService;
	
	@GetMapping
	public ResponseEntity<List<TagDTO>> getTagsByUser(@PathVariable("idUser") Integer idUser) {
		List<Tag> tags = tagService.findAllByUser(idUser);
		List<TagDTO> tagsDTO = new ArrayList<TagDTO>();
		for (Tag t : tags) {
			tagsDTO.add(new TagDTO(t));
		}
		return new ResponseEntity<List<TagDTO>>(tagsDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<TagDTO> getTag(@PathVariable("id") Integer id){
		Tag tag = tagService.findOne(id);
		if(tag == null){
			return new ResponseEntity<TagDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<TagDTO>(new TagDTO(tag), HttpStatus.OK);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<TagDTO> saveTag(@RequestBody TagDTO tagDTO, @PathVariable("idUser") Integer idUser){
		Tag tag = tagDTO.convertToTag();
		User user = userService.findOne(idUser);
		if(user == null) {
			return new ResponseEntity<TagDTO>(HttpStatus.BAD_REQUEST);
		}	
		tag.setName(tagDTO.getName());
		tag.setUser(user);
		tag = tagService.save(tag);
		return new ResponseEntity<TagDTO>(new TagDTO(tag), HttpStatus.CREATED);	
	}
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<TagDTO> updateTag(
			@RequestBody TagDTO tagDTO,
			@PathVariable("id") Integer id,
			@PathVariable("idUser") Integer idUser){
		if(!id.equals(tagDTO.getId())) {
			return new ResponseEntity<TagDTO>(HttpStatus.BAD_REQUEST);
		}	
		Tag tag = tagService.findOne(id); 
		if (tag == null) {
			return new ResponseEntity<TagDTO>(HttpStatus.BAD_REQUEST);
		}
		
		Tag izmenjenTag = tagDTO.convertToTag();
		User user = userService.findOne(idUser);
		izmenjenTag.setUser(user);
		tag = tagService.save(izmenjenTag);	
		return new ResponseEntity<TagDTO>(new TagDTO(tag), HttpStatus.OK);	
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteTag(@PathVariable("id") Integer id){
		Tag tag = tagService.findOne(id);
		if (tag != null){
			tagService.remove(tag);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
}
	
