package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.converter.UserConverter;
import api.dto.UserDTO;
import api.entity.User;
import api.service.UserServiceInterface;

@RestController
@RequestMapping(value="api/users")
public class UserController {
	
	@Autowired
	private UserServiceInterface userService;
	
	@Autowired
	private UserConverter converter;
	
	@GetMapping
	public ResponseEntity<List<UserDTO>> getUsers() {
		List<User> users = userService.findAll();
		List<UserDTO> usersDTO = new ArrayList<UserDTO>();
		for (User u : users) {
			usersDTO.add(converter.toDTO(u));
		}
		return new ResponseEntity<List<UserDTO>>(usersDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<UserDTO> getUser(@PathVariable("id") Integer id){
		User user = userService.findOne(id);
		if(user == null){
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<UserDTO>(converter.toDTO(user), HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/login", consumes ="application/json")
	public ResponseEntity<UserDTO> login(@RequestBody UserDTO userDTO){
		String userName = userDTO.getUserName();
		String password = userDTO.getPassword();
		
		User matchUser = userService.findByUsernameAndPassword(userName, password);
		
		if(matchUser == null){
			return new ResponseEntity<UserDTO>(HttpStatus.NOT_FOUND);
		}

		//TODO: videti da li zelimo celog usera vratiti, ili cemo mu izbaciti password
		return new ResponseEntity<UserDTO>(converter.toDTO(matchUser), HttpStatus.OK);
		
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO userDTO){
		User user = converter.toUser(userDTO);
		user = userService.save(user);
		return new ResponseEntity<UserDTO>(converter.toDTO(user), HttpStatus.CREATED);	
	}
	
	@PutMapping(value="/{id}", consumes="application/json")
	public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO, @PathVariable("id") Integer id){
		
		if(!id.equals(userDTO.getId())) {
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		}
		
		User user = userService.findOne(id); 
		if (user == null) {
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		}
		
		//TODO: Proveriti sa asistentom da li smemo menjati username?
		User izmenjenUser = converter.toUser(userDTO);
		user = userService.save(izmenjenUser);
		
		return new ResponseEntity<UserDTO>(converter.toDTO(user), HttpStatus.OK);	
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable("id") Integer id){
		User user = userService.findOne(id);
		if (user != null){
			userService.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
}
