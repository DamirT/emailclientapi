package api.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.AccountDTO;
import api.dto.FolderDTO;
import api.entity.Account;
import api.entity.Folder;

@Component
public class AccountConverter {
	
	@Autowired
	private FolderConverter fConverter;
	
	public AccountDTO toDTO(Account acc) {
		AccountDTO dto = new AccountDTO();
		dto.setId(acc.getId());
		dto.setUserName(acc.getUserName());
		dto.setPassword(acc.getPassword());
		dto.setDisplayName(acc.getDisplayName());
		
		Set<FolderDTO> foldersDTO = new HashSet<FolderDTO>();
		for (Folder folder : acc.getFolders()) {
			foldersDTO.add(fConverter.toDTO(folder));
		}
		dto.setFolders(foldersDTO);
		return dto;
	}
	
	public Account toAccount(AccountDTO dto) {
		Account account = new Account();
		//ako ikad bude trebala izmena accounta, ovo ce nam znaciti
		if (dto.getId() != null) {
			account.setId(dto.getId());
			//ovde trebam preuzeti folderedto, konvertovati ih i setovati accountu
			for (FolderDTO folderDTO : dto.getFolders()) {
				account.getFolders().add(fConverter.toFolder(folderDTO));
			}
		}else {
			// trebam napraviti nova4 defaultna foldera
			Folder inbox = new Folder("Inbox", account);
			Folder sent = new Folder("Sent", account);
			Folder drafts = new Folder("Drafts", account);
			Folder trash = new Folder("Trash", account);
			account.getFolders().add(inbox);
			account.getFolders().add(sent);
			account.getFolders().add(drafts);
			account.getFolders().add(trash);
		}
		account.setUserName(dto.getUserName());
		account.setPassword(dto.getPassword());
		account.setDisplayName(dto.getDisplayName());
		
		return account;
	}

}
