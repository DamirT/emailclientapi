package api.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.FolderDTO;
import api.dto.RuleDTO;
import api.entity.Folder;
import api.entity.Rule;
import api.service.FolderServiceInterface;

@Component
public class FolderConverter {
	
	@Autowired
	private FolderServiceInterface folderService;
	
	public FolderDTO toDTO(Folder folder) {
		FolderDTO dto = new FolderDTO();
		dto.setId(folder.getId());
		dto.setName(folder.getName());
		
		if(folder.getParent() != null) {
			dto.setParentId(folder.getParent().getId());
		}
		
		dto.setCountOfMessages(folder.getMessages().size());
		
		Set<RuleDTO> rulesDTO = new HashSet<RuleDTO>();
		for (Rule r : folder.getRules()) {
			rulesDTO.add(new RuleDTO(r));
		}
		dto.setRules(rulesDTO);
		
		return dto;
	}
	
	public Folder toFolder(FolderDTO dto) {
		
		Folder folder = new Folder();
		// Ako radim izmenu foldera onda treba prepisati ID
		if(dto.getId() != null) {
			folder.setId(dto.getId());
		}
		folder.setName(dto.getName());
		Integer parentId = dto.getParentId();
		if (parentId != null) {
			Folder parent = folderService.findOne(parentId);
			folder.setParent(parent);
		}

		for (RuleDTO ruleDTO : dto.getRules()) {
			Rule rule = ruleDTO.convertToRule();
			rule.setFolder(folder);
			folder.getRules().add(rule);
		}
		return folder;
	}

}
