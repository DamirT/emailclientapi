package api.converter;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import api.dto.AccountDTO;
import api.dto.TagDTO;
import api.dto.UserDTO;
import api.entity.Account;
import api.entity.Tag;
import api.entity.User;

@Component
public class UserConverter {
	
	@Autowired
	private AccountConverter aConverter;
	
	public UserDTO toDTO(User user) {
		UserDTO dto = new UserDTO();
		dto.setId(user.getId());
		dto.setUserName(user.getUsername());
		dto.setPassword(user.getPassword());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		
		Set<TagDTO> tagsDTO = new HashSet<TagDTO>();
		for(Tag tag : user.getTags()) {
			tagsDTO.add(new TagDTO(tag));
		}
		dto.setTags(tagsDTO);
		
		Set<AccountDTO> accountsDTO = new HashSet<AccountDTO>();
		for(Account account : user.getAccounts()) {
			accountsDTO.add(aConverter.toDTO(account));
		}
		dto.setAccounts(accountsDTO);
		return dto;
	}
	
	public User toUser(UserDTO dto) {
		User user = new User();
		if(dto.getId() != null) {
			user.setId(dto.getId());
		}
		user.setUsername(dto.getUserName());
		user.setPassword(dto.getPassword());
		user.setFirstName(dto.getFirstName());
		user.setLastName(dto.getLastName());
		for (TagDTO tagDTO : dto.getTags()) {
			Tag tag = tagDTO.convertToTag();
			tag.setUser(user);
			user.getTags().add(tag);
		}
		for (AccountDTO accountDTO : dto.getAccounts()) {
			Account account = aConverter.toAccount(accountDTO);
			account.setUser(user);
			user.getAccounts().add(account);
		}
		return user;
	}

}
