package api.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class AccountDTO implements Serializable{
	private static final long serialVersionUID = 1L;
		
	private Integer id;
	private String userName;
	private String password;
	private String displayName;
	private Set<FolderDTO> folders = new HashSet<FolderDTO>();
	
	public AccountDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Set<FolderDTO> getFolders() {
		return folders;
	}

	public void setFolders(Set<FolderDTO> folders) {
		this.folders = folders;
	}
	
}
