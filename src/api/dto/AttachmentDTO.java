package api.dto;

import java.io.Serializable;
import java.util.Base64;

import api.entity.Attachment;

public class AttachmentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String data;
	
	private String mime_type;
	
	private String name;
	
	
	public AttachmentDTO() {
		super();
	}
	
	public AttachmentDTO(Attachment attachment){
		this.id = attachment.getId();
		this.data = Base64.getEncoder().encodeToString(attachment.getData());
		this.mime_type = attachment.getMime_type();
		this.name = attachment.getName();
	}

	public static AttachmentDTO convertToLightAttachment(Attachment attachmet) {
		AttachmentDTO attachmentDTO = new AttachmentDTO();
		attachmentDTO.setId(attachmet.getId());
		attachmentDTO.setName(attachmet.getName());
		return attachmentDTO;
		}
	
	public Attachment convertToAttachment() {
		Attachment attachment = new Attachment();
		//ako bi trebala izmena attachmenta, ovo ce biti vazno, mada mislim da se to nikad nece raditi
		if (this.getId() != null) {
			attachment.setId(this.getId());
		}
		byte [] data = Base64.getMimeDecoder().decode(this.getData());
		attachment.setData(data);
		attachment.setMime_type(mime_type);
		attachment.setName(name);
		return attachment;
	}
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMime_type() {
		return mime_type;
	}

	public void setMime_type(String mime_type) {
		this.mime_type = mime_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
