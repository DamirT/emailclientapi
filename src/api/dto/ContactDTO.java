package api.dto;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;

import api.entity.Contact;
import api.entity.Photo;

public class ContactDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	final String SLIKE_PATH = "slike";
	
	
	private Integer id;
	
	private String firstName;
	
	private String lastName;
	
	private String displayName;
	
	private String email;
	
	private String note;
	
	private String photo;
	
	
	
	public ContactDTO() {
		super();
	}

	
//	File slika = new File(SLIKE_PATH + "/slika1.png");


	public ContactDTO(Contact contact) {
		this.id = contact.getId();
		this.firstName = contact.getFirstName();
		this.lastName = contact.getLastName();
		this.displayName = contact.getDisplayName();
		this.email = contact.getEmail();
		this.note = contact.getNote();
		if(contact.getPhoto() != null) {
			String path = SLIKE_PATH + "/" + contact.getPhoto().getPath();
			this.photo = getEncodedImage(path);
		}
	}
	
	
	
	public Contact convertToContact() {
		Contact contact = new Contact();
		if(this.getId() != null) {
			contact.setId(this.getId());
		}
		contact.setFirstName(this.getFirstName());
		contact.setLastName(this.getLastName());
		contact.setDisplayName(this.getDisplayName());
		contact.setEmail(this.getEmail());
		contact.setNote(this.getNote());
		if(this.getPhoto() != null) {
			String photoName = getImagePath(this.getPhoto());
			Photo photo = new Photo();
			photo.setPath(photoName);
			photo.setContact(contact);
			contact.setPhoto(photo);
		}
		return contact;
	}
	
	private String getEncodedImage(String path) {
		File image = new File(path);
		try {
			byte [] photoB = Files.readAllBytes(image.toPath());
			return Base64.getEncoder().encodeToString(photoB);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private String getImagePath(String photo) {
		byte[] photoB = Base64.getDecoder().decode(photo);
		String photoName = UUID.randomUUID().toString();
		Path path = Paths.get(SLIKE_PATH + "/" + photoName);
		try {
			Files.write(path, photoB);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return photoName;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}




	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getDisplayName() {
		return displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}	
	
	
	
}
