package api.dto;

import java.util.HashSet;
import java.util.Set;

public class FolderDTO {
	
	private Integer id;
	private String name;
	private Integer parentId;
	private int countOfMessages;
	private Set<RuleDTO> rules = new HashSet<RuleDTO>();
	
	public FolderDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public int getCountOfMessages() {
		return countOfMessages;
	}

	public void setCountOfMessages(int countOfMessages) {
		this.countOfMessages = countOfMessages;
	}

	public Set<RuleDTO> getRules() {
		return rules;
	}

	public void setRules(Set<RuleDTO> rules) {
		this.rules = rules;
	}

}
