package api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import api.entity.Attachment;
import api.entity.Message;
import api.entity.Tag;
import api.util.DateConverter;


public class MessageDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String from;
	private String to;
	private String cc;
	private String bcc;
	private String dateTimeString;
	private String subject;
	private String content;
	private Boolean unread;
	private Integer folderId;
	private Set<AttachmentDTO> attachments = new HashSet<AttachmentDTO>();
	private Set<TagDTO> tags = new HashSet<TagDTO>();
	
	public MessageDTO() {
		super();
	}
	
	public Message convertToMessage() {
		Message message = new Message();
		
		if(this.getId() != null) {
			message.setId(this.getId());
			
		}
		message.setFrom(this.getFrom());
		message.setTo(this.getTo());
		message.setCc(this.getCc());
		message.setBcc(this.getBcc());
		Date dateTime = DateConverter.fromISO8601UTC(this.dateTimeString);
		message.setDateTime(dateTime);
		message.setSubject(this.getSubject());
		message.setContent(this.getContent());
		message.setUnread(this.getUnread());
		// folder setujem u kontroleru
		Set<Attachment> attachments = new HashSet<Attachment>();
		for (AttachmentDTO attachmentDTO : this.getAttachments()) {
			Attachment attachment = attachmentDTO.convertToAttachment();
			attachment.setMessage(message);
			attachments.add(attachment);
		}	
		message.setAttachments(attachments);
		return message;		
	}
	
	public MessageDTO(Message message){
		this.id = message.getId();
		this.from = message.getFrom();
		this.to = message.getTo();
		this.cc = message.getCc();
		this.bcc = message.getBcc();
		this.dateTimeString = DateConverter.toISO8601UTC(message.getDateTime());
		this.subject = message.getSubject();
		this.content = message.getContent();
		this.unread = message.getUnread();
		this.folderId = message.getFolder().getId();
		Set<AttachmentDTO> attachmentsDTO = new HashSet<AttachmentDTO>();
		for (Attachment attachment : message.getAttachments()) {
			attachmentsDTO.add(AttachmentDTO.convertToLightAttachment(attachment));
		}
		this.attachments = attachmentsDTO;
		Set<TagDTO> tagsDTO = new HashSet<TagDTO>();
		for (Tag tag : message.getTags()) {
			tagsDTO.add(new TagDTO(tag));
		}
		this.tags = tagsDTO;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getDateTimeString() {
		return dateTimeString;
	}

	public void setDateTimeString(String dateTimeString) {
		this.dateTimeString = dateTimeString;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getUnread() {
		return unread;
	}

	public void setUnread(Boolean unread) {
		this.unread = unread;
	}

	public Integer getFolderId() {
		return folderId;
	}

	public void setFolderId(Integer folderId) {
		this.folderId = folderId;
	}

	public Set<AttachmentDTO> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<AttachmentDTO> attachments) {
		this.attachments = attachments;
	}

	public Set<TagDTO> getTags() {
		return tags;
	}

	public void setTags(Set<TagDTO> tags) {
		this.tags = tags;
	}	
}
