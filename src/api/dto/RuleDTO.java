package api.dto;

import api.entity.Condition;
import api.entity.Operation;
import api.entity.Rule;

public class RuleDTO {
	
	private Integer id;
	private Condition condition;
	private String value;
	private Operation operation;
	
	public RuleDTO() {
		super();
	}

	public RuleDTO(Integer id, Condition condition, String value, Operation operation) {
		super();
		this.id = id;
		this.condition = condition;
		this.value = value;
		this.operation = operation;
	}
	
	public RuleDTO(Rule rule) {
		this(rule.getId(), rule.getCondition(), rule.getValue(), rule.getOperation());
	}
	
	public Rule convertToRule() {
		Rule rule = new Rule();
		// Pri konverziji DTO u Rule vodimo racuna da se sacuvaju postojeci ID-jevi
		if(this.getId() != null) {
			rule.setId(this.getId());
		}
		rule.setCondition(this.getCondition());
		rule.setValue(this.getValue());
		rule.setOperation(this.getOperation());
		return rule;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	
	
}
