package api.dto;

import java.io.Serializable;
import api.entity.Tag;

public class TagDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	

	public TagDTO() {
		super();
	}

	public TagDTO(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;		
	}
	
	public TagDTO(Tag tag) {
		this(tag.getId(), tag.getName());
	}
	
	public Tag convertToTag() {
		Tag tag = new Tag();
		if(this.getId() != null) {
			tag.setId(this.id);
		}
		tag.setName(this.getName());
		
		return tag;
		
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
