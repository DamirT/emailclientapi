package api.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="attachments")
public class Attachment implements Serializable {
	private static final long serialVersionUID = 1L;

	  @Id
	  @GeneratedValue(strategy=IDENTITY)
	  @Column(name="attachment_id", unique=true, nullable=false)
	  private Integer id;
	  
	  @Column(name="data", unique=false, nullable=false, columnDefinition = "longblob")
	  private byte[] data;
	  
	  @Column(name="mime_type", unique=false, nullable=false)
	  private String mime_type;
	  
	  @Column(name="name", unique=false, nullable=false)
	  private String name;
	  
	  
	  // VEZE
	  
	  @ManyToOne
	  @JoinColumn(name = "message_id", referencedColumnName = "message_id", nullable = false)
	  private Message message;
	  
	  public Attachment() {
		  
	  }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getMime_type() {
		return mime_type;
	}

	public void setMime_type(String mime_type) {
		this.mime_type = mime_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
	
	public Attachment copyAttachment() {
		
		Attachment copyAttachment = new Attachment();
		copyAttachment.setName(this.getName());
		copyAttachment.setData(this.getData());
		copyAttachment.setMime_type(this.getMime_type());
		return copyAttachment;
	}

}
