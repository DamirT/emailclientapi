package api.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="messages")
public class Message implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=IDENTITY)
	@Column(name="message_id", unique=true, nullable=false)
	private Integer id;
	
	@Column(name="from_sender", unique=false, nullable=false)
	private String from;
	
	@Column(name="to_receiver", unique=false, nullable=true, columnDefinition = "text")
	private String to;	
	
	@Column(name="cc", unique=false, nullable=true, columnDefinition = "text")
	private String cc;
	
	@Column(name="bcc", unique=false, nullable=true, columnDefinition = "text")
	private String bcc;
	
	@Column(name="dateTime", unique=false, nullable=false)
	private Date dateTime;
	
	@Column(name="subject", unique=false, nullable=true)
	private String subject;
	
	@Column(name="content", unique=false, nullable=true, columnDefinition = "mediumtext")
	private String content;
	
	@Column(name="unread", unique=false, nullable=false)
	private Boolean unread;
	
	@ManyToOne
	@JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
	private Account account;

	@ManyToOne
	@JoinColumn(name = "folder_id", referencedColumnName = "id", nullable = false)
	private Folder folder;

	@OneToMany(cascade = { ALL }, fetch = LAZY, mappedBy = "message", orphanRemoval = true)
	private Set<Attachment> attachments = new HashSet<Attachment>();
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable (
			name = "tag_message",
			joinColumns = { @JoinColumn(name = "message_id") },
			inverseJoinColumns = {@JoinColumn(name = "tag_id") })
	private Set<Tag> tags = new HashSet<Tag>();

	
	public Message() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Boolean getUnread() {
		return unread;
	}

	public void setUnread(Boolean unread) {
		this.unread = unread;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}
			
	public Set<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<Attachment> attachments) {
		this.attachments = attachments;
	}
		
	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}
	
	public Message copyMessage() {
		
		//id i fodler ne kopiramo, jer ID baza dodeljuje, a folder se setuje tamo gde se trazi kopija poruke
		
		Message copyMessage = new Message();
		copyMessage.setFrom(this.getFrom());
		copyMessage.setTo(this.getTo());
		copyMessage.setCc(this.getCc());
		copyMessage.setBcc(this.getBcc());
		copyMessage.setDateTime(this.getDateTime());
		copyMessage.setSubject(this.getSubject());
		copyMessage.setContent(this.getContent());
		copyMessage.setUnread(this.getUnread());
		copyMessage.setAccount(this.getAccount());
		Set<Attachment> attachments = new HashSet<Attachment>();
		for (Attachment attachment : this.getAttachments()) {
			Attachment copyAttachment = attachment.copyAttachment();
			attachments.add(copyAttachment);
		}
		copyMessage.setAttachments(attachments);
		return copyMessage;
	}
	
}
