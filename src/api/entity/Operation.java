package api.entity;

public enum Operation {
	
	MOVE, COPY, DELETE
}
