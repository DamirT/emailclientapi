package api.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.FetchType.EAGER;

@Entity
@Table(name="users")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy=IDENTITY)
  @Column(name="user_id", unique=true, nullable=false)
  private Integer id;
  
  @Column(name="username", unique=true, nullable=false)
  private String username;
  
  @Column(name="password", unique=false, nullable=false)
  private String password;
  
  @Column(name="first_name", unique=false, nullable=false)
  private String firstName;
  
  @Column(name="last_name", unique=false, nullable=false)
  private String lastName;
   
  @OneToMany(cascade= {ALL}, fetch=EAGER, mappedBy="user")
  private Set<Account> accounts = new HashSet<Account>();
  
  @OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="user", orphanRemoval=true)
  private Set<Tag> tags = new HashSet<Tag>();
  
  public User() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Set<Account> getAccounts() {
	return accounts;
  }
	
  public void setAccounts(Set<Account> accounts) {
	this.accounts = accounts;
  }
	
  public Set<Tag> getTags() {
	return tags;
  }
	
  public void setTags(Set<Tag> tags) {
	this.tags = tags;
  }
  
}
