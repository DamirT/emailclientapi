package api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import api.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {
	
	List<Account> findByUser_id(Integer id);

}
