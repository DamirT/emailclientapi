package api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import api.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {
	
	Attachment findByName(String name);
	
	List<Attachment> findByMessage_id(Integer id);

}
