package api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import api.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer> {

	List<Contact> findByUser_id(Integer id);
	
}
