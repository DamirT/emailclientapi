package api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import api.entity.Folder;

public interface FolderRepository extends JpaRepository<Folder, Integer>  {
	
	List<Folder> findByParent_id(Integer id);
	
	List<Folder> findByAccount_id(Integer id);

}
