package api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import api.entity.Account;
import api.entity.Message;


public interface MessageRepository extends JpaRepository<Message, Integer>{

	Message findById(Integer id);
	
	List<Message> findByFolder_id(Integer id);
	
	List<Message> findByAccount_id(Integer accId);
	
	Message findTopByAccountOrderByDateTimeDesc(Account account);
	
	
	
	
}
