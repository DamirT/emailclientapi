package api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import api.entity.Tag;

public interface TagRepository extends JpaRepository<Tag, Integer> {
	
	Tag findByName(String name);
	
	List<Tag> findByUser_id(Integer id);

}
