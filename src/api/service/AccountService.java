package api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.entity.Account;
import api.repository.AccountRepository;


@Service
public class AccountService implements AccountServiceInterface {

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	FolderService folderService;
	
	@Override
	public List<Account> findAllByUser(Integer id) {
		return accountRepository.findByUser_id(id);
	}

	@Override
	public Account findOne(Integer id) {
		return accountRepository.findOne(id);
	}

	@Override
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Override
	@Transactional
	public void remove(Account account) {
		
		//nema potrebe da brisem foldere koji spadaju u ovaj acc, brisu se automatski zbog bidirekcije
		// ali je zato potrebno pre brisanja ukloniti acc iz liste accounta kod usera
		account.getUser().getAccounts().remove(account);
		accountRepository.delete(account);
	}

}
