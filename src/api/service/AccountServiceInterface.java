package api.service;

import java.util.List;

import api.entity.Account;


public interface AccountServiceInterface {

	List<Account> findAllByUser(Integer id);
	
	Account findOne(Integer id);
	
	Account save(Account account);
	
	void remove(Account account);
	
}
