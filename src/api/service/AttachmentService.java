package api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.entity.Attachment;
import api.repository.AttachmentRepository;

@Service
public class AttachmentService implements AttachmentServiceInterface {
	
	@Autowired
	AttachmentRepository attachmentRepository;
	
	@Override
	public List<Attachment> findAllByMessage(Integer id) {
		return attachmentRepository.findByMessage_id(id);
	}
	
	@Override
	public Attachment findOne(Integer id) {
		return attachmentRepository.findOne(id);
	}
	
	
	@Override
	public Attachment save(Attachment attachment) {
		return attachmentRepository.save(attachment);
	}

	@Override
	@Transactional
	public void remove(Attachment attachment) {
		attachment.getMessage().getAttachments().remove(attachment);
		attachmentRepository.delete(attachment.getId());
	}
}
