package api.service;

import java.util.List;

import api.entity.Attachment;

public interface AttachmentServiceInterface {
	
	List<Attachment> findAllByMessage(Integer id);
	
	Attachment findOne(Integer id);
	
	Attachment save(Attachment attachment);
	
	void remove(Attachment attachment);
	

}
