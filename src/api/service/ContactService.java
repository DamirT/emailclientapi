package api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.entity.Contact;
import api.repository.ContactRepository;

@Service
public class ContactService implements ContactServiceInterface{
	
	@Autowired
	ContactRepository contactRepository;

	@Override
	public List<Contact> findAllByUser(Integer id) {
		return contactRepository.findByUser_id(id);
	}

	@Override
	public Contact findOne(Integer id) {
		return contactRepository.findOne(id);
	}

	@Override
	public Contact save(Contact contact) {
		return contactRepository.save(contact);
	}

	@Override
	public void remove(Integer id) {
		contactRepository.delete(id);
	}
	
}
