package api.service;

import java.util.List;

import api.entity.Contact;

public interface ContactServiceInterface {
	
	List<Contact> findAllByUser(Integer id);
	
	Contact findOne(Integer id);
	
	Contact save(Contact contact);
	
	void remove(Integer id);
	
	
}
