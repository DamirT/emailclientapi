package api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.entity.Folder;
import api.repository.FolderRepository;


@Service
public class FolderService implements FolderServiceInterface {
	
	@Autowired
	private FolderRepository folderRepository;

	@Override
	public List<Folder> findAllByAcc(Integer id) {
		return folderRepository.findByAccount_id(id);
	}

	@Override
	public Folder findOne(Integer id) {
		return folderRepository.findOne(id);
	}

	@Override
	public Folder save(Folder folder) {
		return folderRepository.save(folder);
	}

	@Override
	@Transactional
	public void remove(Folder folder) {
		
		List<Folder> children = folderRepository.findByParent_id(folder.getId());
		for (Folder child : children) {
			remove(child);
		}
		//nema potrebe rucno brisati poruke koje pripadaju folderu, zbog bidirekcije brisu se automatski
		
		//da bi brisanje uspelo moram prvo ovaj folder izbaciti iz liste foldera u accoutnt-u
		folder.getAccount().getFolders().remove(folder);
		folderRepository.delete(folder);
	}

}
