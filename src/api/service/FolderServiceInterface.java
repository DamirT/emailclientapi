package api.service;

import java.util.List;

import api.entity.Folder;

public interface FolderServiceInterface {
	
	List<Folder> findAllByAcc(Integer id);
	
	Folder findOne(Integer id);
	
	Folder save(Folder folder);
	
	void remove(Folder folder);

}
