package api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.sun.mail.imap.IMAPFolder.FetchProfileItem;

import api.entity.Account;
import api.entity.Attachment;
import api.entity.Folder;
import api.entity.Message;
import api.entity.Operation;
import api.entity.Rule;
import api.entity.User;
import api.util.MailUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.AndTerm;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;

@Service
public class MailReaderService {
	Logger log = LoggerFactory.getLogger(MailReaderService.class);

	private static final long ONE_MINUTE = 60 * 1000;
	private static final long TEN_SECONDS = 10 * 1000;
	

	@Autowired
	private UserServiceInterface userService;

	@Autowired
	private AccountServiceInterface accountService;

	@Autowired
	private MessageServiceInterface messageService;

	@Scheduled(fixedRate = ONE_MINUTE, initialDelay = TEN_SECONDS)
	public void loadMessagesToDatabase() {

		List<User> users = userService.findAll();
		for (User user : users) {
			readUserMessages(user);
		}
	}
	
	private void readUserMessages(User user) {
		
		List<Account> accounts = accountService.findAllByUser(user.getId());
		for (Account account : accounts) {
			readAccountMessages(account);
		}
	}
	
	private void readAccountMessages(Account account) {
		//Prvo pronaci poslednji sacuvan mail u bazi za account
		Message lastMessage = messageService.findLastMessagge(account);
		saveMessagesFromServer(account, lastMessage);
	}

	private void saveMessagesFromServer(Account account, Message lastMessage) {

		// Potrebno je sve poruke za pocetak sacuvati u Inbox-u
		Folder inbox = findInboxFolder(account.getFolders());
		if (inbox == null) {
			log.error("Ne postoji INBOX folder! Ovo se nikad ne bi smelo desiti!");
			return;
		}

		try {

			// Spremanje property-ja koji se koriste za konekciju i citanje poruka na serveru
			Properties properties = new Properties();

			properties.put("mail.imap.host", account.getInServerAddress());
			properties.put("mail.imap.port", account.getInServerPort());
			properties.put("mail.imap.starttls.enable", "true");
			properties.put("mail.imap.partialfetch","false");
			properties.put("mail.imap.fetchsize", "524288"); //pola MB
			properties.put("mail.imaps.partialfetch", "false"); 
			properties.put("mail.imaps.fetchsize", "524288"); 
			Session emailSession = Session.getDefaultInstance(properties);

			// create the IMAP store object and connect with the imap server
			Store store = emailSession.getStore("imaps");

			// MDA
			store.connect(account.getInServerAddress(), account.getUserName(), account.getPassword());

			// create the folder object and open it
			javax.mail.Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(javax.mail.Folder.READ_ONLY);
			List<javax.mail.Message> messages;
			
			// Zelimo dovuci samo najnovije mailove (zato nam je potreban nas "lastMessage")
			if (lastMessage != null) {
				//Zbog nedovoljne preciznosti IMAP-a moram uzeti veci scope (naknadno odbacujem duple)
				Calendar cal = Calendar.getInstance();
				cal.setTime(lastMessage.getDateTime());
				cal.add(Calendar.DAY_OF_MONTH, -1);
				SearchTerm newerThan = new ReceivedDateTerm(ComparisonTerm.GT, cal.getTime());
				SearchTerm unread = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
				SearchTerm andTerm = new AndTerm(newerThan, unread);
				javax.mail.Message[] msgs= emailFolder.search(andTerm);
				
				//s obzirom da message ne dovuce datum, pa u dodatnoj proveri prouzrokuje jos mnogo odlazaka na mail server, 
				//ovo je nacin da mi ipak dovucemo i podatak o datumu u svakom mail-u...
				FetchProfile fp = new FetchProfile();
				fp.add(FetchProfileItem.INTERNALDATE);
				
				emailFolder.fetch(msgs, fp);
				messages = Arrays.asList(msgs);
				messages = additionalMessageCheck(messages, lastMessage.getDateTime());
			}else {
				// Preuzimamo sve poruke iz Inbox-a (nasa baza je prazna)
				messages = Arrays.asList(emailFolder.getMessages());
			}

			for (javax.mail.Message message: messages) {
				Message msg = new Message();
				msg.setFrom(addressListToString(message.getFrom()));
				msg.setTo(addressListToString(message.getRecipients(RecipientType.TO)));
				msg.setCc(addressListToString(message.getRecipients(RecipientType.CC)));
				msg.setBcc(addressListToString(message.getRecipients(RecipientType.BCC)));
				msg.setDateTime(message.getReceivedDate());
				msg.setSubject(message.getSubject());
				msg.setContent(MailUtil.getTextFromMessage(message));
				msg.setUnread(!message.isSet(Flags.Flag.SEEN));
				msg.setAccount(account);
				msg.setFolder(inbox);
				Set<Attachment> attachments = MailUtil.extractAttachments(message);
				for (Attachment att : attachments) {
					att.setMessage(msg);
				}
				msg.setAttachments(attachments);
				System.out.println("Preuzet mail: " + account.getUserName()+": "+msg.getSubject()+"; broj atacmenta:"+attachments.size());
				saveMessageWithIncludedRules(msg);
			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<javax.mail.Message> additionalMessageCheck(List<javax.mail.Message> messages, Date lastMessageDate) throws MessagingException {
		
		List<javax.mail.Message> result = new ArrayList<>();
		for (javax.mail.Message message : messages) {
			if(lastMessageDate.before(message.getReceivedDate())) {
				result.add(message);
			}
		}
		return result;
	}

	private Folder findInboxFolder(Set<Folder> folders) {
		for (Folder folder : folders) {
			if ("Inbox".equals(folder.getName())) {
				return folder;
			}
		}
		return null;
	}

	private String addressListToString(Address[] address) {

		// Ako je lista adresa null ili je prazna onda vracamo null
		if (address == null || address.length == 0) {
			return null;
		}

		/**
		 * 1. Pravimo Stream od niza "address" 2. Svaki element niza mapiramo u nesto
		 * sto mi zelimo, u ovom slucaju zelimo da ga pretvorimo u String 3. Skupljamo
		 * sve stringove koji su "mapirani" u listu "addressNames"
		 **/
		List<String> addressNames = Stream.of(address).map(Address::toString).collect(Collectors.toList());
		return String.join(",", addressNames);
	}
	
	private void saveMessageWithIncludedRules(Message message) {
		
		ArrayList<Rule> rules = getAllRulesForAccount(message);
		ArrayList<Rule> messagesRules = getRulesForMessage(message, rules);
		
		if (hasOperationDelete(message, messagesRules)) {
			messageService.save(message);
		}
		else {
			operationCopy(message, messagesRules);
			
			if(operationMove(message, messagesRules)) {
				messageService.save(message);
			}
			else {
				messageService.save(message);
			}
		}	
	}
	
	private ArrayList<Rule> getAllRulesForAccount(Message message){
		
		Set<Folder> folders = message.getAccount().getFolders();
		ArrayList<Rule> allRules = new ArrayList<Rule>();
		for (Folder folder : folders) {
			allRules.addAll(folder.getRules());
		}
		return allRules;
		
	}
	
	private ArrayList<Rule> getRulesForMessage(Message message, ArrayList<Rule> rules){
		
		ArrayList<Rule> machRules = new ArrayList<Rule>();
		for (Rule rule : rules) {
			switch (rule.getCondition().toString()) {
			case "TO":
				if (message.getTo().toLowerCase().contains(rule.getValue().toLowerCase())) {
					machRules.add(rule);
				}
				break;
			case "FROM":
				if (message.getFrom().toLowerCase().contains(rule.getValue().toLowerCase())) {
					machRules.add(rule);
				}
				break;
			case "CC":
				if (message.getCc().toLowerCase().contains(rule.getValue().toLowerCase())) {
					machRules.add(rule);
				}
				break;
			case "SUBJECT":
				if (message.getSubject().toLowerCase().contains(rule.getValue().toLowerCase())) {
					machRules.add(rule);
				}
				break;
			default:
				break;
			}
		}
		return machRules;
		
	}
	
	private boolean hasOperationDelete(Message message, ArrayList<Rule> messagesRules) {
		boolean operationDelete = false;
		Folder trash = findFolderTrashForAccount(message.getAccount());
		for (Rule rule : messagesRules) {
			if (Operation.DELETE.equals(rule.getOperation())) {
				message.setFolder(trash);
				operationDelete = true;
				break;
			}
		}
		return operationDelete;
	}

	private Folder findFolderTrashForAccount(Account account) {
		for (Folder folder : account.getFolders()) {
			if (("Trash").equals(folder.getName())) {
				return folder;
			}
		}
		return null;
	}
	
	private void operationCopy(Message message, ArrayList<Rule> messagesRules) {
		
		for (Rule rule : messagesRules) {
			if (Operation.COPY.equals(rule.getOperation())) {
				Message copyMessage = message.copyMessage();
				copyMessage.setFolder(rule.getFolder());
				messageService.save(copyMessage);
			}
		}
	}
	
	private boolean operationMove(Message message, ArrayList<Rule> messagesRules) {
		boolean operationMove = false;
		for (Rule rule : messagesRules) {
			if (Operation.MOVE.equals(rule.getOperation())) {
				message.setFolder(rule.getFolder());
				operationMove = true;
				break;
			}
		}
		return operationMove;
	}
	
	
}
