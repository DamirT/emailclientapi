package api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import api.entity.Account;
import api.entity.Attachment;
import api.entity.Message;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

@Service
public class MailSenderService {
	Logger log = LoggerFactory.getLogger(MailSenderService.class);

	// SLANJE MAIL-A NA SERVER!

	public void sendMessageOnMailServer(Message message) {

		Account account = message.getAccount();

		// Spremanje property-ja
		Properties prop = new Properties();
		prop.put("mail.smtp.port", account.getSmtpPort());
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true");

		// Spremanje mail-a za slanje
		Session session = Session.getDefaultInstance(prop, null);
		MimeMessage mail = new MimeMessage(session);
		try {
			mail.addRecipients(javax.mail.Message.RecipientType.TO, message.getTo());
			mail.addRecipients(javax.mail.Message.RecipientType.CC, message.getCc());
			mail.addRecipients(javax.mail.Message.RecipientType.BCC, message.getBcc());
			mail.setSubject(message.getSubject());

			//content se sastoji iz vise delova, pored texta, ostali delovi su zapravo attachmenti
			//prvo preuzmemo textualni deo content-a
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(message.getContent());

			// multipart je "kofer" za sve ostale delove, pa je potrebno te delove upakovati u njega
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			//postoji mogucnosst da ce biti vise attachmenata pa onda moramo svaki spakovati u poseban deo
			for (Attachment attachment : message.getAttachments()) {
				messageBodyPart = new MimeBodyPart();
				messageBodyPart.setDataHandler(new DataHandler(attachment.getData(), attachment.getMime_type()));
				messageBodyPart.setFileName(attachment.getName());
				multipart.addBodyPart(messageBodyPart);
			}
			//ovde mail-u setujemo kompletiran content (sadrzaj: text + attachment/i)
			mail.setContent(multipart);

			// Slanje mail-a
			Transport transport = session.getTransport("smtps");
			transport.connect(account.getSmtpAddress(), account.getUserName(), account.getPassword());
			transport.sendMessage(mail, mail.getAllRecipients());
			transport.close();

		} catch (MessagingException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

}
