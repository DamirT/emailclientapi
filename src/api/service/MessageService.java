package api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.entity.Account;
import api.entity.Folder;
import api.entity.Message;
import api.entity.Tag;
import api.repository.FolderRepository;
import api.repository.MessageRepository;


@Service
public class MessageService implements MessageServiceInterface {

	@Autowired
	MessageRepository messageRepository;
	
	@Autowired
	FolderRepository folderRepository;
	
	@Autowired
	MailSenderService mailSenderService;
	
	@Override
	public List<Message> findAllByFolder(Integer id) {
		return messageRepository.findByFolder_id(id);
	}
	
	@Override
	public Message findOne(Integer id) {
		Message message = messageRepository.findOne(id);
		message.setUnread(false);
		return messageRepository.save(message);
	}
	
	@Override
	@Transactional
	public Message saveAndSend(Message message) {
		Message mess =  messageRepository.save(message);
		mailSenderService.sendMessageOnMailServer(mess);
		return mess;
	}
	
	@Override
	public Message save(Message message) {
		return messageRepository.save(message);
	}
	
	@Override
	public void remove(Message message) {	
		if (("Trash").equals(message.getFolder().getName())) {
			message.getFolder().getMessages().remove(message);
			for (Tag tag : message.getTags()) {		
				tag.getMessages().remove(message);
			}
			message.getTags().clear();
			messageRepository.delete(message);
		}else {		
			Set<Folder> folders = message.getAccount().getFolders();
			Folder folder = null;
			for (Folder f : folders) {
				if(("Trash").equals(f.getName())) {
					folder = f;
					break;					
				}							
			}
			message.setFolder(folder);
			save(message);
		}
	}
	
	public Message findLastMessagge(Account account) {
		return messageRepository.findTopByAccountOrderByDateTimeDesc(account);
	}

	@Override
	public List<Message> findOnlyNewMessages(Integer accId, Integer maxId) {

		List<Message> allMessages = messageRepository.findByAccount_id(accId);
		List<Message> foundMessages = new ArrayList<Message>();
		for (Message message : allMessages) {
			String folderName = message.getFolder().getName();
			if(isFolderOK(folderName) && message.getId() > maxId) {
				foundMessages.add(message);
			}
		}
		return foundMessages;
	}
	
	private boolean isFolderOK(String folderName) {
		if ("Sent".equals(folderName) || "Drafts".equals(folderName) || "Trash".equals(folderName)) {
			return false;
		}
		return true;
	}

	@Override
	public Message saveDraftsMessage(Message message) {
		return messageRepository.save(message);
	}
	
}
