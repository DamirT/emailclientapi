package api.service;

import java.util.List;

import api.entity.Account;
import api.entity.Message;


public interface MessageServiceInterface {

	
	List<Message> findAllByFolder(Integer id);
	
	List<Message> findOnlyNewMessages(Integer accId, Integer maxId);
	
	Message findOne(Integer id);
	
	Message saveAndSend(Message message);
	
	Message save(Message message);
	
	Message saveDraftsMessage(Message message);
	
	void remove(Message message);
	
	Message findLastMessagge(Account account);

	
}
