package api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.entity.Message;
import api.entity.Tag;
import api.repository.TagRepository;
@Service
public class TagService implements TagServiceInterface {
	
	@Autowired
	TagRepository tagRepository;
	
	@Override
	public List<Tag> findAllByUser(Integer id) {
		return tagRepository.findByUser_id(id);
	}
	
	@Override
	public Tag findOne(Integer id) {
		return tagRepository.findOne(id);
	}
		
	@Override
	public Tag save(Tag tag) {
		return tagRepository.save(tag);
	}

	@Override
	@Transactional
	public void remove(Tag tag) {
		for (Message message : tag.getMessages()) {
			message.getTags().remove(tag);
		}
		tag.getUser().getTags().remove(tag);
		tagRepository.delete(tag);
	}
}
