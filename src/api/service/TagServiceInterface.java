package api.service;

import java.util.List;

import api.entity.Tag;


public interface TagServiceInterface {
	
	List<Tag> findAllByUser(Integer id);
	
	Tag findOne(Integer id);

	Tag save(Tag tag);
	
	void remove(Tag tag);

}
