package api.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.entity.Account;
import api.entity.Contact;
import api.entity.User;
import api.repository.AccountRepository;
import api.repository.ContactRepository;
import api.repository.UserRepository;

@Service
public class UserService implements UserServiceInterface {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ContactRepository contactRepository;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}
	
	@Override
	public User findOne(Integer id) {
		return userRepository.findOne(id);
	}
	
	@Override
	public User findByUsernameAndPassword(String username, String password){
		User user = userRepository.findByUsername(username);
		if(user.getPassword().equals(password))
			return user;
		else
			return null;
	}
	
	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public void remove(Integer id) {
		List<Contact> contactsByUser = contactRepository.findByUser_id(id);
		for (Contact contact : contactsByUser) {
			contactRepository.delete(contact.getId());
		}
		
		List<Account> accountsByUser = accountRepository.findByUser_id(id);
		for (Account account : accountsByUser) {
			accountRepository.delete(account.getId());
		}
		
		userRepository.delete(id);
	}
}
