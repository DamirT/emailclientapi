package api.service;

import java.util.List;

import api.entity.User;

public interface UserServiceInterface {
	
	List<User> findAll();
	
	User findOne(Integer id);
	
	User findByUsernameAndPassword(String username, String password);
	
	User save(User user);
	
	void remove(Integer id);
}
