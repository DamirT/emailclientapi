package api.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeMultipart;

import org.springframework.util.StringUtils;

import api.entity.Attachment;

public class MailUtil {
	
	public static String getTextFromMessage(Message message) throws IOException, MessagingException {
	    String result = "";
	    if (message.isMimeType("text/plain")) {
	        result = message.getContent().toString();
	    } else if (message.isMimeType("multipart/*")) {
	        MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
	        result = getTextFromMimeMultipart(mimeMultipart);
	    }
	    return result;
	}

	private static String getTextFromMimeMultipart(
	        MimeMultipart mimeMultipart) throws IOException, MessagingException {

	    int count = mimeMultipart.getCount();
	    if (count == 0)
	        throw new MessagingException("Multipart with no body parts not supported.");
	    boolean multipartAlt = new ContentType(mimeMultipart.getContentType()).match("multipart/alternative");
	    if (multipartAlt)
	        // alternatives appear in an order of increasing 
	        // faithfulness to the original content. Customize as req'd.
	        return getTextFromBodyPart(mimeMultipart.getBodyPart(count - 1));
	    String result = "";
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        result += getTextFromBodyPart(bodyPart);
	    }
	    return result;
	}

	private static String getTextFromBodyPart(
	        BodyPart bodyPart) throws IOException, MessagingException {
	    
	    String result = "";
	    if (bodyPart.isMimeType("text/plain")) {
	        result = (String) bodyPart.getContent();
	    } else if (bodyPart.isMimeType("text/html")) {
	        String html = (String) bodyPart.getContent();
	        result = html; //org.jsoup.Jsoup.parse(html).text();
	    } else if (bodyPart.getContent() instanceof MimeMultipart){
	        result = getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
	    }
	    return result;
	}
	
	public static Set<Attachment> extractAttachments(Message message) throws MessagingException, IOException{
	
		Set<Attachment> attachments = new HashSet<Attachment>();
		String contentType = message.getContentType();
		if(!(contentType.contains("multipart"))) { // ako contentType nije multipart onda ovde nema attachmenta
			return attachments;
		}
		
		Multipart multipart = (Multipart) message.getContent();

	    for (int i = 0; i < multipart.getCount(); i++) {
	        BodyPart bodyPart = multipart.getBodyPart(i);
	        if(!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) && StringUtils.isEmpty(bodyPart.getFileName())) {
	            continue; // Ako bodyPart nije ATTACHMENT i ako nema fileName onda preskacemo ovaj part
	        } 

	        System.out.println("Citam attachment maila: "+message.getSubject()+", attName: "+ bodyPart.getFileName());
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	      
	        int bytesRead;
	        byte[] buff = new byte[16384];
	        InputStream is = bodyPart.getInputStream();
	        while ((bytesRead = is.read(buff, 0, buff.length)) != -1) {
	          baos.write(buff, 0, bytesRead);
	        }
	        
	        byte[] data  = baos.toByteArray();
	        baos.close();
	        System.out.println("Velicina fajla: " + data.length);
	        
	        Attachment att = new Attachment();
	        att.setName(bodyPart.getFileName());
	        att.setData(data);
	        att.setMime_type(bodyPart.getContentType());
	        attachments.add(att);
	    }
		return attachments;
	}

}
